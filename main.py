import pickle
import time
import random
import string
import datetime
import numpy as np
import copy
import matplotlib #pylint: disable=import-error
import sys
#to be able to plot without any windowserver
matplotlib.use('Agg')
import matplotlib.pyplot as plt #pylint: disable=import-error
import itertools as it
import math

#debug
# import pdb
# np.random.seed(0)
# import warnings
# warnings.filterwarnings("error")

#constants
datasetFolder = 'cifar-10-batches-py/'
resultsFolder = 'results/'
classLabels = ['Airplane', 'Automobile', 'Bird', 'Cat', 'Deer', 'Dog', 'Frog','Horse', 'Ship', 'Truck']
K = 10
omega = 0.001
bn_epsilon = 1e-6 #winged it
#Layer information
# hidden_nodes = [50, 50, 50, K]
# hidden_nodes = [50, 30, K]
hidden_nodes = [50, K]
# hidden_nodes = [10, K]
number_of_layers = len(hidden_nodes)
# use_batch_normalization = False
use_batch_normalization = True


class GDparams:
    def __init__(self, n_batch,eta,epochs,rho):
        self.n_batch = n_batch
        self.eta = eta
        self.epochs = epochs
        self.rho = rho

class momentum:
    def __init__(self,vW1,vW2,vb1,vb2):
        self.vW1 = vW1
        self.vW2 = vW2
        self.vb1 = vb1
        self.vb2 = vb2

class result_container:
    def __init__(self,eta,lamb,acc,run_id):
        self.eta = eta
        self.lamb = lamb
        self.acc = acc
        self.run_id = run_id

class layer_container:

    def __init__(self, input_nodes, output_nodes, omega):
        # Init matrixes
        self.W = np.random.normal(loc=0.0, scale=omega, size=(output_nodes, input_nodes))
        self.b = np.zeros([1, output_nodes])
        self.vW = np.zeros_like(self.W)
        self.vb = np.zeros_like(self.b)
        self.grad_W = np.zeros_like(self.W)
        self.grad_b = np.zeros_like(self.b)
        self.latest_s1 = None
        self.latest_s1_hat = None
        self.latest_h = None
        self.avg_mean = None
        self.avg_variance = None
        self.latest_mean = None
        self.latest_variance = None
        #parameters
        self.bn_alpha = 0.99

    def update_gradient(self, gd):
        self.vW = (gd.rho * self.vW) + (gd.eta * self.grad_W)
        self.vb = (gd.rho * self.vb) + (gd.eta * self.grad_b)

        self.W = self.W - self.vW
        self.b = self.b - self.vb

    def update_avg_mean(self, mean):
        if self.avg_mean is None:
            self.avg_mean = mean
        else:
            self.avg_mean = (self.bn_alpha * self.avg_mean) + ((1 - self.bn_alpha)*mean)

    def update_avg_variance(self, variance):
        if self.avg_variance is None:
            self.avg_variance = variance
        else:
            self.avg_variance = (self.bn_alpha * self.avg_variance) + ((1 - self.bn_alpha)*variance)

def main():
    #Different run types
    # Just for testing
    # trainAndEvaluate(100,0.02,30,1,0,1000,1000,"best-settings-but-k-layer")
    # trainAndEvaluate(100,0.02,1,0,0,1000,1000,"test-one-epoch")
    # trainAndEvaluate(100, 0.1, 1, 0, 10000, 10000,"test")
    # trainAndEvaluate(100,0.05,20,0,0.5,10000,10000,"momentum-rho1")
    # trainAndEvaluate(100,0.05,20,0,0.9,10000,10000,"momentum-rho2")
    # trainAndEvaluate(100,0.05,20,0,0.99,10000,10000,"momentum-rho3")

    #Exercise 4 - Training your network
    #finding Ranges for eta
    # trainAndEvaluate(100,0.01,10,0.000001,0.9,10000,10000,"broad-search-eta4")
    # trainAndEvaluate(100,0.005,10,0.000001,0.9,10000,10000,"broad-search-eta5")
    # trainAndEvaluate(100,0.001,10,0.000001,0.9,10000,10000,"broad-search-eta6")

    #fine course searching
    '''
    eta_min = math.log(0.2)
    eta_max = math.log(0.01)
    lambda_min = math.log(0.0001)
    lambda_max = math.log(0.01)
    #save for results
    results = []
    course_file = open("courseResultsBN2.txt","w")
    course_file.write("Running 10 experiments a 20 epochs\n")
    course_file.write("Eta = [%f, %f]\n" % (eta_min, eta_max))
    course_file.write("Lambda = [%f, %f]\n" % (lambda_min, lambda_max))
    rs = ""
    for i in range(20): 
        rand_eta_exp = eta_min + (eta_max - eta_min)*np.random.random_sample()
        rand_lambda_exp = lambda_min + (lambda_max - lambda_min)*np.random.random_sample()
        rand_eta = math.exp(rand_eta_exp)
        rand_lambda = math.exp(rand_lambda_exp)
        print("rand_eta: %f rand lambda: %f" % (rand_eta, rand_lambda))
        run_id,acc,v_cost,cost = trainAndEvaluate(100,rand_eta,20,rand_lambda,0.99,10000,10000,"rand-course-nb-2-test%d" % i,True)
        stats = result_container(rand_eta,rand_lambda,acc,run_id)
        results.append(stats)

    course_file.write("----------------------------\n")
    course_file.write("Results sorted by Acc:\n")
    course_file.write("----------------------------\n")
    results.sort(key=lambda x: x.acc, reverse=True)
    for res in results:
        course_file.write("%s: Eta: %f Lamda: %f Acc: %f\n" % (res.run_id, res.eta, res.lamb, res.acc))
    course_file.close()
    '''

    # running speed test momentum
    # trainAndEvaluate(100,0.02,10,0.00001,0.9,10000,10000,"speed-test-with-momentum")
    # trainAndEvaluate(100,0.02,10,0.00001,0,10000,10000,"speed-test-without-momentum")


    #Running with best settings
    # best_eta = 0.0245
    # best_lambda = 0.0009
    # trainAndEvaluate(100,best_eta,20,best_lambda, 0.99,10000,10000,"3-layer-50-30-10-bn-best-settings")
    # trainAndEvaluate(100,0.0001,20,0.000001, 0.99,40000,10000,"3-layer-50-30-10-EvenLower2Lambda-Eta-Low-Lambda")
    # trainAndEvaluate(100,0.0001,20,0.001, 0.99,40000,10000,"3-layer-50-30-10-EvenLower2Lambda-Eta-High-Lamda")
    # Run 2 layer with batch normilazation and and 3 different learning rates (low medium high)
    # Low = 0.001 Medium = 0.0245 High = 0.05
    eta_low = 0.001
    eta_medium = 0.0245
    eta_high = 0.1
    # trainAndEvaluate(100,eta_low,10,0.0009,0.99,10000,10000,"2-layer-eta-low-without-bn", False)
    # trainAndEvaluate(100,eta_medium,10,0.0009,0.99,10000,10000,"2-layer-eta-medium-without-bn", False)
    trainAndEvaluate(100,eta_high,10,0.0009,0.99,10000,10000,"2-layer-eta-high-without-bn", False)

    # best_eta = 0.245
    # best_lambda = 0.000001
    # # best_lambda = 0.0
    # rho = 0.99
    # trainAndEvaluate(100,best_eta,30,best_lambda,rho,10000,10000,"best-settings-but-k-layer-k-is-4-higher-eta")

    #Fine tuning lambda
    # trainAndEvaluate(100,0.003,30,0.0007,0.9,10000,10000,"testing-higher-eta")
    # trainAndEvaluate(100,0.007,20,0.000001,0.9,10000,10000,"fuck-this-shit")
    # trainAndEvaluate(100,0.016892,10,0.000001,0.9,10000,10000,"with-zero-mean")

def trainAndEvaluate(n_batch,eta,epochs,lamb,rho,nTrain,nVal,label=None,noGraph=False):
    runId = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    if label is not None:
        runId = '%s-%s' % (label, runId)
    print("== Started run %s ==" % runId)
    timeStarted = datetime.datetime.now()
    tX,tY,ty,vX,vY,vy = loadData(nTrain, nVal)
    testX, testY, testy = loadTestData()
    #Zero mean 
    mean_a = tX.mean()
    tX = tX - mean_a
    vX = vX - mean_a
    testX = testX - mean_a

    dimension = tX.shape[1]
    # Check to see if we are testing gradients
    if len(sys.argv) > 1:
        dimension = 10

    print("# Init matrices....")
    layers = []
    for i in range(number_of_layers):
        #special case for first layer since 
        #we are using input data
        if(i == 0):
            new_layer = layer_container(dimension, hidden_nodes[i],omega)
        else:
            new_layer = layer_container(hidden_nodes[i-1], hidden_nodes[i],omega)
        layers.append(new_layer)
    print('# Init complete.')

    #CHeck to see if we want gradients 
    if len(sys.argv) > 1:
        smallX = tX[:10]
        smallY = tY[:10]
        smally = ty[:10]
        #cut dimension of x to 50
        smallX = smallX[:,:dimension]
        lamb = 0
        print("COMPARING GRADIENTS")
        layers_2 = copy.copy(layers)
        Ws, bs = numericalGradientSlow(smallX, smally, layers_2, lamb, 1e-5)
        print("Computed numerical gradients")
        computeGradients(smallX,smallY,layers,lamb)
        print("Computed analytic gradients")
        # quit()
        diffGradToComputed(layers, Ws, bs)
        print("done")
        quit()

    #params
    gd = GDparams(n_batch,eta,epochs,rho)
    savedCost = []
    savedCostValidation = []
    savedAcc = []
    #string to save results in
    rs = ""
    print("# Running %d epochs:" % gd.epochs)
    #check that cost is like random guessing at first
    print("  -- Cost before we start : %f" % computeCost(tX,ty,layers,lamb))
    print("  -- Vali Cost before we start : %f" % computeCost(vX,vy,layers,lamb))

    for i in range(gd.epochs):
        print("    Running epoch %d of %d:" % (i+1,gd.epochs), end='')
        rs += ("-----------\n")
        rs += ("  Epoch: %d\n" % (i+1))
        rs += ("  - Running minibatch...\n")
        layers = miniBatchGD(tX,tY,gd,layers,lamb)
        rs += ("  - Computing cost...\n")
        cost = computeCost(tX,ty,layers,lamb)
        savedCost.append(cost)
        print(" Cost: %f" % cost, end='')
        rs += ("  - Computing validation cost...\n")
        valicost = computeCost(vX,vy,layers,lamb)
        savedCostValidation.append(valicost)
        print(" V-Cost: %f" % valicost, end='')
        rs += ("  - Computing accuracy...\n")
        acc = computeAccuracy(testX,testy,layers)
        savedAcc.append(acc)
        print(" Acc: %f" % acc)
        rs += ("  Cost: %f\n" % cost)
        rs += ("  Vali-cost: %f\n" % valicost)
        rs += ("  Acc : %f\n" % acc)

    print("# %d epochs completed." % gd.epochs)
    if noGraph is False:
        plotFigures(savedCost, savedCostValidation, runId)
    print("# Saving results to %s-results.txt" % runId)
    resultsFile = open('%s%s-results.txt' % (resultsFolder, runId), 'w')
    resultsFile.write("Run with id: %s\n\n" % runId)
    resultsFile.write("Started %s\n" % timeStarted)
    resultsFile.write("Finished %s\n" % datetime.datetime.now())
    resultsFile.write("n_batch:%d\neta:%f\n" % (n_batch, eta))
    resultsFile.write("epochs :%d\nlambda:%f\n" % (epochs, lamb))
    resultsFile.write("rho: %f" % gd.rho)
    resultsFile.write("Training set size: %d\n" % nTrain)
    resultsFile.write("Validation set size: %d\n" % nVal)
    resultsFile.write("Final accuracy: %f\n" % savedAcc[-1])
    resultsFile.write("\n\nSaved epochs information:\n%s" % rs)
    resultsFile.close()
    print("==Done with %s ==" % runId)
    return runId,savedAcc[-1],savedCostValidation[-1],savedCost[-1]

def plotFigures(savedCost, savedCostValidation, runId):
    print("# Plotting figure...")
    L = range(len(savedCost))
    plt.plot([i+1 for i in L], savedCost, '-b', label='Training cost')
    plt.plot([i+1 for i in L],savedCostValidation, 'r-', label='Validation cost')
    plt.ylabel('Cost')
    plt.xlabel('Epoch')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, mode="expand", borderaxespad=0.)
    plt.savefig('%s%s-graph.png' % (resultsFolder, runId))
    plt.clf()
    plt.cla()
    plt.close()
    print("# Saved figure as %s-graph.png" % runId)

def miniBatchGD(X,Y,gd,layers,lamb):
    #go in steps of n_batch
    #0..99, 100..199 etc
    for i in range(0, X.shape[0], gd.n_batch):
        #extract rows
        Xbatch = X[i:i + gd.n_batch]
        Ybatch = Y[i:i + gd.n_batch]
        layers = computeGradients(Xbatch,Ybatch,layers,lamb)

        for i in range(len(layers)):
            layers[i].update_gradient(gd)
    #done
    # learning rate descrese
    gd.eta = gd.eta * 0.95
    return layers

def computeGradients(X,Y,layers,lamb):
    batchSize = X.shape[0]
    dim = X.shape[1]
    p = evaluateClassifier(X,layers)

    #reset all gradients (they can be zero or not)
    for l in range(len(layers)):
        layers[l].grad_W = np.zeros_like(layers[l].grad_W)
        layers[l].grad_b = np.zeros_like(layers[l].grad_b)

    #p is now K x n 
    #ie every column is one image
    p = p.T
    #every row is an image
    all_g = []
    for i in range(batchSize):
        all_g.append((p[i] - Y[i]).T)
    all_g = np.array(all_g)
    #go backwards through the layers
    #Start with the final layer:
    for l in range(len(layers) - 1, 0, -1):
        #Grads
        layers[l].grad_b = (1/float(batchSize)) * np.sum(all_g, axis=0)
        layers[l].grad_b = layers[l].grad_b.reshape(1, layers[l].grad_b.shape[0])
        for i in range(batchSize):
            one_h = layers[l - 1].latest_h.T[i].reshape(hidden_nodes[l-1], 1)
            one_g = all_g[i].reshape(1, all_g.shape[1])
            layers[l].grad_W = layers[l].grad_W + (one_g.T @ one_h.T)
        layers[l].grad_W = layers[l].grad_W / float(batchSize)
        layers[l].grad_W = layers[l].grad_W + (2 * float(lamb) * layers[l].W)
        #Back prop
        new_g = []
        for i in range(batchSize):
            one_g = all_g[i].reshape(1, all_g.shape[1])
            temp_g = one_g @ layers[l].W
            diag = np.diagflat([1 if x > 0 else 0 for x in layers[l - 1].latest_s1_hat.T[i]])
            new_g.append(temp_g @ diag)
        all_g = np.array(new_g)
        all_g = all_g.reshape(all_g.shape[0], all_g.shape[2])
        if use_batch_normalization:
            all_g = batch_normalize_backwards(all_g, layers[l-1].latest_s1, layers[l-1].latest_mean, layers[l-1].latest_variance)
    #last layer
    layers[0].grad_b = (1/float(batchSize)) * np.sum(all_g, axis=0)
    layers[0].grad_b = layers[0].grad_b.reshape(1, layers[0].grad_b.shape[0])
    for i in range(batchSize):
        one_x = X[i].reshape([dim, 1])
        one_g = all_g[i].reshape(1, all_g.shape[1])
        layers[0].grad_W = layers[0].grad_W + (one_g.T @ one_x.T)
    layers[0].grad_W = (1/float(batchSize)) * layers[0].grad_W + (2 * float(lamb) * layers[0].W)

    return layers

def computeAccuracy(X,y,layers):
    #each row is p for one image
    p = evaluate(X,layers)
    p = p.T
    hits = 0
    for i in range(p.shape[0]):
        if np.argmax(p[i]) == y[i]:
            hits += 1
    return hits/(X.shape[0])

def computeCost(X,y,layers,lamb):
    #for each x calculate cross-entropy loss
    # print("IN compute cost, X shape: %s" % str(X.shape))
    p = evaluate(X,layers)
    p = p.T
    sumOfLoss = 0
    for i in range(p.shape[0]):
        sumOfLoss += -np.log(p[i][y[i]])
    sumOfLoss /= float(X.shape[0])
    #calculate W^2
    sumWs = 0
    for l in range(len(layers)):
        sumWs += np.sum(np.square(layers[l].W))
    return sumOfLoss + (lamb * sumWs)

def evaluateClassifier(X,layers):
    XT = copy.copy(X.T)
    for lay in layers:
        lay.latest_s1 = (lay.W @ XT) + lay.b.T
        #Compute mean and variance for batch normilazation
        #for each batch
        mean = (1 / float(lay.latest_s1.shape[1])) * lay.latest_s1.sum(axis=1)
        lay.update_avg_mean(mean)
        lay.latest_mean = mean
        #variance
        variance = np.zeros_like(mean)
        for n_dim in range(lay.latest_s1.shape[0]):
            sum_row = 0
            for n_batch in range(lay.latest_s1.shape[1]):
                sum_row += np.power(lay.latest_s1[n_dim][n_batch] - mean[n_dim], 2)
            variance[n_dim] = (1/float(lay.latest_s1.shape[1])) * sum_row
        lay.update_avg_variance(variance)
        lay.latest_variance = variance
        #do da batch
        if use_batch_normalization:
            lay.latest_s1_hat = batch_normalize(lay.latest_s1,mean,variance)
        else:
            lay.latest_s1_hat = lay.latest_s1
        lay.latest_h = np.maximum(lay.latest_s1_hat,0)
        XT = lay.latest_h
    #last layer we dont use h
    #perform softmax
    s = layers[-1].latest_s1
    p = np.exp(s) / (np.ones([1,K]) @ np.exp(s))
    return p

'''
Equation:
grad S_i = 
[1]        g_i @ diag(v_l + e)^(-1/2) 
[2]      + (2/n)(-1/2 sum(g_i @ diag(v_l + e)^(-3/2) @ diag(s_i - my_l))) @ diag(s_i - my_i)
[3]      - (1/n) sum(gi @ diag(v_l + e)^(-1/2))
'''
def batch_normalize_backwards(g,s,mean,variance):
    batchSize = g.shape[0]
    sT = copy.copy(s.T)
    g_J_g_my = None
    V_b_1 = np.diagflat(np.float_power((variance + bn_epsilon), float(-1/2)))
    for i in range(batchSize):
        if g_J_g_my is None:
            g_J_g_my = g[i] @ V_b_1
        else:
            g_J_g_my = g_J_g_my + (g[i] @ V_b_1)
    g_J_g_my = float(-1) * g_J_g_my
    
    g_J_g_v = None
    V_b_3 = np.diagflat(np.float_power((variance + bn_epsilon), float(-3/2)))
    for i in range(batchSize):
        if g_J_g_v is None:
            g_J_g_v = g[i] @ V_b_3 @ np.diagflat(sT[i] - mean)
        else:
            g_J_g_v = g_J_g_v + (g[i] @ V_b_3 @ np.diagflat(sT[i] - mean))
    g_J_g_v = float(-1/2) * g_J_g_v

    m = []
    for i in range(batchSize):
        val = (g[i] @ V_b_1) 
        val += (2.0/batchSize) * (g_J_g_v @ np.diagflat(sT[i] - mean))
        val += g_J_g_my * (float(1/batchSize))
        m.append(val)
    return np.array(m).reshape(g.shape[0], g.shape[1])


def batch_normalize(s, mean, variance):
    #s, mean and variance should be the same dimensions
    sT = copy.copy(s.T)
    m = []
    diag = np.diagflat(np.float_power(variance + bn_epsilon, float(-1/2)))
    for i in range(sT.shape[0]):
        m.append(diag @ (sT[i] - mean))
    return np.array(m).T

'''
Same as evaulate classifier but does not save any values
in the layers
'''
def evaluate(X,layers):
    XT = copy.copy(X.T)
    for lay in layers:
        s1 = (lay.W @ XT) + lay.b.T
        #Compute mean and variance for batch normilazation
        if lay.avg_mean is not None and lay.avg_variance is not None:
            s1_hat = batch_normalize(s1, lay.avg_mean, lay.avg_variance)
            XT = np.maximum(s1_hat,0)
        else:
            XT = np.maximum(s1, 0)
    #last layer we dont use h
    #perform softmax
    s = s1
    p = np.exp(s) / (np.ones([1,K]) @ np.exp(s))
    return p

#Loads all data and cuts where appropriate
def loadData(nTrain, nVal):
    print('# Loading data...')
    filePrefix = 'data_batch_'
    X,Y,y = loadBatch('%s%d' % (filePrefix, 1))
    for i in range(2,6):
        nX,nY,ny = loadBatch('%s%d' % (filePrefix, i))
        X = np.append(X, nX, axis=0)
        Y = np.append(Y, nY, axis=0)
        y = np.append(y, ny, axis=0)
    #cut it up
    if nVal + nTrain > X.shape[0]:
        print(" ! loadData - to many images requested, exiting !")
        quit()
    st = nTrain
    so = nTrain+nVal
    print('# Loaded %d train and %d validation images.' % (nTrain, nVal))
    return X[:st],Y[:st],y[:st],X[st:so],Y[st:so],y[st:so]

#loads the test batch
def loadTestData():
    print('# Loading test data...')
    X,Y,y = loadBatch('test_batch')
    print('# Loaded test data.')
    return X,Y,y

def loadBatch(batchname):
    d = unpickle(datasetFolder + batchname)
    X = d[b'data']
    # We have values between 0-255 we want
    # between 0 and 1
    X = X * (1/255)
    Y = np.zeros([X.shape[0], K])
    y = d[b'labels']
    # one-hot encoding
    for i in range(X.shape[0]):
        Y[i][y[i]] = 1
    return X,Y,y

#From where we got the image files
def unpickle(f):
    with open(f, 'rb') as fo:
        d = pickle.load(fo, encoding='bytes')
    return d

#function to check computed gradient vs analytic
def diffGradToComputed(layers, Ws, bs):
    eps = 0.0001
    #len(layers) = len(Ws) = len(bs)
    print("in diff")
    for l in range(len(layers)):
        print("Comparing layer %d:" % l)

        #first for b
        print("For b")
        # print("\tAnalytic:\tNumeric:")
        print("\tRelative error:")
        b_shape = layers[l].b.shape
        for i,j in it.product(range(b_shape[0]), range(b_shape[1])):
            print("[%d,%d]\t%e\t%e" % (i,j,layers[l].grad_b[i][j], bs[l][i][j]))
            ab, nb = layers[l].grad_b[i][j], bs[l][i][j]
            if ab == 0.0 and nb == 0.0:
                val = 0.0
            else:
                val = abs(ab - nb)/(max(abs(ab), abs(nb)))
            print("[%d,%d]Rel\t%e" % (i,j,val))



        print("For W")
        # print("\tAnalytic:\tNumeric:\tNumpy:")
        print("\tRelative error:")
        W_shape = layers[l].W.shape
        # npW = np.gradient(layers[l].W)[1]
        for i,j in it.product(range(W_shape[0]), range(W_shape[1])):
            print("[%d,%d]\t%e\t%e" % (i,j,layers[l].grad_W[i][j], Ws[l][i][j]))
            aW, nW = layers[l].grad_W[i][j], Ws[l][i][j]
            #check grad
            any_zero = "no"
            if aW == 0.0 and nW == 0.0:
                val = 0.0
                any_zero = "Yes"
            else:
                val = abs(aW - nW)/max(abs(aW), abs(nW))
            print("[%d,%d]Rel\t%e\tAny_Zero:%s" % (i,j,val,any_zero))
        print("-----------------------------------")
        _ = input("Enter for next")
            



#Numerical gradient, from the ComputeGradsNumSlow.m
def numericalGradientSlow(X,y,layers,lamb,h):
    list_grad_Ws = []
    list_grad_bs = []
    layer_counter = 0
    for lay in layers:
        gW = np.zeros_like(lay.W)
        gb = np.zeros_like(lay.b)
        print("\tFor layer %d calculating grad_b..." % layer_counter)
        for i,j in it.product(range(gb.shape[0]), range(gb.shape[1])):
            b_try = copy.copy(lay.b)
            b_try[i,j] = b_try[i,j] - h
            new_layer = copy.copy(layers)
            new_layer[layer_counter].b = b_try
            c1 = computeCost(X,y,new_layer,lamb)
            b_try = copy.copy(lay.b)
            b_try[i,j] = b_try[i,j] + h
            new_layer = copy.copy(layers)
            new_layer[layer_counter].b = b_try
            c2 = computeCost(X,y,new_layer,lamb)
            gb[i,j] = (c2-c1) / (2*h)

        print("\tFor layer %d calculating grad_W..." % layer_counter)
        for i,j in it.product(range(gW.shape[0]), range(gW.shape[1])):
            W_try = copy.copy(lay.W)
            W_try[i,j] = W_try[i,j] - h
            new_layer = copy.copy(layers)
            new_layer[layer_counter].W = W_try
            c1 = computeCost(X,y,new_layer,lamb)
            W_try = copy.copy(lay.W)
            W_try[i,j] = W_try[i,j] + h
            new_layer = copy.copy(layers)
            new_layer[layer_counter].W = W_try
            c2 = computeCost(X,y,new_layer,lamb)
            gW[i,j] = (c2-c1) / (2*h)

        list_grad_Ws.append(gW)
        list_grad_bs.append(gb)
        layer_counter += 1

    return list_grad_Ws, list_grad_bs

if __name__ == "__main__":
    main()
