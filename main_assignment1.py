import pickle
import time
import random
import string
import datetime
import numpy as np
import copy
import matplotlib
#to be able to plot without any windowserver
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import itertools as it

#constants
datasetFolder = 'cifar-10-batches-py/'
resultsFolder = 'results/'
K = 10
omega = 0.01
classLabels = ['Airplane', 'Automobile', 'Bird', 'Cat', 'Deer', 'Dog', 'Frog','Horse', 'Ship', 'Truck']

class GDparams:
    def __init__(self, n_batch, eta, epochs):
        self.n_batch = n_batch
        self.eta = eta
        self.epochs = epochs

def main():
    #Different run types
    # Just for testing
    # trainAndEvaluate(100, 0.1, 1, 0, 10000, 10000,"test")

    #Original test for the report, uses batch1 as training data
    #and batch2 as validation data
    # '''
    trainAndEvaluate(100, 0.1 , 40, 0  , 10000, 10000, "NewOriginalTest1")
    trainAndEvaluate(100, 0.01, 40, 0  , 10000, 10000, "NewOriginalTest2")
    trainAndEvaluate(100, 0.01, 40, 0.1, 10000, 10000, "NewOriginalTest3")
    trainAndEvaluate(100, 0.01, 40, 1  , 10000, 10000, "NewOriginalTest4")
    # '''

    #Grid search with the rest as original parameters
    '''
    lambdas = [0.0001, 0.00001, 0.000001]
    etas = [0.2,  0.1, 0.05]
    for i, j in it.product(range(3),range(3)):
        trainAndEvaluate(100, etas[i], 40, lambdas[j], 10000, 10000,"gridSearch-e-%g-l-%g" %(etas[i], lambdas[j]))
    '''

    #Running with all data and original parameters to see improvement
    '''
    trainAndEvaluate(100, 0.1 , 40, 0  , 40000, 10000, "UsingAllData40000TrainTest1")
    trainAndEvaluate(100, 0.01, 40, 0  , 40000, 10000, "UsingAllData40000TrainTest2")
    trainAndEvaluate(100, 0.01, 40, 0.1, 40000, 10000, "UsingAllData40000TrainTest3")
    trainAndEvaluate(100, 0.01, 40, 1  , 40000, 10000, "UsingAllData40000TrainTest4")
    '''

    #Running original parameters with more epochs to see if there is a
    #difference
    '''
    trainAndEvaluate(100, 0.1 , 150, 0  , 10000, 10000, "150EpochsTest1")
    trainAndEvaluate(100, 0.01, 150, 0  , 10000, 10000, "150EpochsTest2")
    trainAndEvaluate(100, 0.01, 150, 0.1, 10000, 10000, "150EpochsTest3")
    trainAndEvaluate(100, 0.01, 150, 1  , 10000, 10000, "150EpochsTest4")
    '''

    #Run a few test with all of the training data an better parameters
    # '''
    # trainAndEvaluate(100, 0.007 , 40, 0.01  , 49000, 1000, "OptimizedTest5")
    # trainAndEvaluate(100, 0.007 , 40, 0.1  , 49000, 1000, "OptimizedTest6")
    # '''

def trainAndEvaluate(n_batch, eta, epochs, lamb, nTrain, nVal, label=None):
    runId = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    if label is not None:
        runId = '%s-%s' % (label, runId)
    print("== Started run %s ==" % runId)
    timeStarted = datetime.datetime.now()
    tX,tY,ty,vX,vY,vy = loadData(nTrain, nVal)
    testX, testY, testy = loadTestData()
    dimension = tX.shape[1]
    print("Number of images: %d" % tX.shape[0])

    print("# Init W and b...")
    (W, b) = initWandB(dimension, omega)
    print('# W and b complete.')

    '''
    #temp code for checking gradients
    smallX = tX[:10]
    smallY = tY[:10]
    smally = ty[:10]
    lamb = 0
    grad_W, grad_b = computeGradients(smallX, smallY, W, b, lamb)
    c_grad_W, c_grad_b = numericalGradientSlow(smallX, smally, W, b, lamb, 1e-6)
    diffGradToComputed(grad_W, grad_b, c_grad_W, c_grad_b, smallX) 
    quit()
    '''

    #params
    gd = GDparams(n_batch, eta, epochs)
    savedCost = []
    savedCostValidation = []
    savedAcc = []
    #string to save results in
    rs = ""
    print("# Running %d epochs:" % gd.epochs)

    for i in range(gd.epochs):
        print("    Running epoch %d of %d..." % (i+1,gd.epochs))
        rs += ("-----------\n")
        rs += ("  Epoch: %d\n" % (i+1))
        rs += ("  - Running minibatch...\n")
        (W, b) = miniBatchGD(tX, tY, gd, W, b, lamb)
        rs += ("  - Computing cost...\n")
        cost = computeCost(tX, ty, W, b, lamb)
        savedCost.append(cost)
        rs += ("  - Computing validation cost...\n")
        valicost = computeCost(vX, vy, W, b, lamb)
        savedCostValidation.append(valicost)
        rs += ("  - Computing accuracy...\n")
        acc = computeAccuracy(testX, testy, W, b)
        savedAcc.append(acc)
        rs += ("  Cost: %f\n" % cost)
        rs += ("  Vali-cost: %f\n" % valicost)
        rs += ("  Acc : %f\n" % acc)

    print("# %d epochs completed." % gd.epochs)
    plotFigures(savedCost, savedCostValidation, runId)
    plotWeightImages(W, runId)
    print("# Saving results to %s-results.txt" % runId)
    resultsFile = open('%s%s-results.txt' % (resultsFolder, runId), 'w')
    resultsFile.write("Run with id: %s\n\n" % runId)
    resultsFile.write("Started %s\n" % timeStarted)
    resultsFile.write("Finished %s\n" % datetime.datetime.now())
    resultsFile.write("n_batch:%d\neta:%f\n" % (n_batch, eta))
    resultsFile.write("epochs :%d\nlambda:%f\n" % (epochs, lamb))
    resultsFile.write("Training set size: %d\n" % nTrain)
    resultsFile.write("Validation set size: %d\n" % nVal)
    resultsFile.write("Final accuracy: %f\n" % savedAcc[-1])
    resultsFile.write("\n\nSaved epochs information:\n%s" % rs)
    resultsFile.close()
    print("==Done with %s ==" % runId)

def plotFigures(savedCost, savedCostValidation, runId):
    print("# Plotting figure...")
    L = range(len(savedCost))
    plt.plot([i+1 for i in L], savedCost, '-b', label='Training cost')
    plt.plot([i+1 for i in L],savedCostValidation, 'r-', label='Validation cost')
    plt.ylabel('Cost')
    plt.xlabel('Epoch')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, mode="expand", borderaxespad=0.)
    plt.savefig('%s%s-graph.png' % (resultsFolder, runId))
    plt.clf()
    plt.cla()
    plt.close()
    print("# Saved figure as %s-graph.png" % runId)

def plotWeightImages(W, runId):
    print("# Plotting weight images...")
    fig, ax = plt.subplots(nrows=2, ncols=5)
    for i in range(K):
        rowCopy = copy.copy(W[i])
        rowCopy = (rowCopy - np.min(rowCopy))/(np.max(rowCopy)-np.min(rowCopy))
        #Thanks John Wikman for help with placing the pixels in the right place! 
        img = np.ndarray((32,32,3))
        for row,col in it.product(range(32), range(32)):
            img[row][col][0] = rowCopy[row*32+col]
            img[row][col][1] = rowCopy[row*32+col+1024]
            img[row][col][2] = rowCopy[row*32+col+2048]

        plt.subplot(2,5,i+1)
        plt.title('%s' % classLabels[i])
        plt.axis('off')
        plt.imshow(img)
    
    plt.savefig('%s%s-weight-images.png' % (resultsFolder, runId))
    plt.clf()
    plt.cla()
    plt.close()
    print("# Saved images as %s-weight-images.png" % runId)

def miniBatchGD(X, Y, gd, W, b, lamb):
    #go in steps of n_batch
    #0..99, 100..199 etc
    for i in range(0, X.shape[0], gd.n_batch):
        #extract rows
        Xbatch = X[i:i + gd.n_batch]
        Ybatch = Y[i:i + gd.n_batch]
        (grad_W, grad_b) = computeGradients(Xbatch,Ybatch,W,b,lamb)
        
        W = W - (grad_W * gd.eta)
        b = b - (grad_b * gd.eta)
    #done
    return (W, b)
    

def computeGradients(X,Y,W,b,lamb):
    batchSize = X.shape[0]
    dimensions = X.shape[1]
    grad_W = np.zeros([K, dimensions])
    grad_b = np.zeros([K, 1])

    p = evaluateClassifier(X, W, b)
    #p is now K x n 
    #ie every column is one image
    for i in range(p.shape[0]):
        #reshape to fit the definition in the slides
        one_p = copy.copy(((p.T)[i]).reshape([K,1]))
        one_y = copy.copy((Y[i]).reshape([K,1]))
        one_x = copy.copy((X[i]).reshape([dimensions, 1]))
        #one_p is Kx1
        #no need for transpose since one_p and one_y
        #took care of it
        g = -(one_y - one_p)

        grad_b =  grad_b + g
        grad_W = grad_W + (g @ one_x.T)

    grad_W /= batchSize
    grad_W =  grad_W + (2 * lamb * W)
    grad_b /= batchSize
    return grad_W, grad_b

def computeAccuracy(X, y, W, b):
    #each row is p for one image
    p = evaluateClassifier(X, W, b).T
    hits = 0
    for i in range(p.shape[0]):
        if np.argmax(p[i]) == y[i]:
            hits += 1
    print(hits)
    return hits/(X.shape[0])

def computeCost(X, y, W, b, lamb):
    #for each x calculate cross-entropy loss
    p = evaluateClassifier(X, W, b).T
    sumOfLoss = 0
    for i in range(p.shape[0]):
        sumOfLoss += -np.log(p[i][y[i]])
    sumOfLoss /= X.shape[0]
    #calculate W^2
    sumW = np.sum(np.square(W))
    return sumOfLoss + (lamb * sumW)

def evaluateClassifier(X, W, b):
    s = (W @ X.T) + b
    #perform softmax
    s = np.exp(s)
    p = s / (np.ones([1,K]) @ s)
    return p

def initWandB(d, omega):
    W = np.zeros([K,d])
    b = np.zeros([K,1])
    for i in range(K):
        b[i,0] = np.random.normal(0.0, omega)
        for j in range(d):
            W[i,j] = np.random.normal(0.0, omega)
    return (W, b)

#Loads all data and cuts where appropriate
def loadData(nTrain, nVal):
    print('# Loading data...')
    filePrefix = 'data_batch_'
    X,Y,y = loadBatch('%s%d' % (filePrefix, 1))
    for i in range(2,6):
        nX,nY,ny = loadBatch('%s%d' % (filePrefix, i))
        X = np.append(X, nX, axis=0)
        Y = np.append(Y, nY, axis=0)
        y = np.append(y, ny, axis=0)
    #cut it up
    if nVal + nTrain > X.shape[0]:
        print(" ! loadData - to many images requested, exiting !")
        quit()
    st = nTrain
    so = nTrain+nVal
    print('# Loaded %d train and %d validation images.' % (nTrain, nVal))
    return X[:st],Y[:st],y[:st],X[st:so],Y[st:so],y[st:so]

#loads the test batch
def loadTestData():
    print('# Loading test data...')
    X,Y,y = loadBatch('test_batch')
    print('# Loaded test data.')
    return X,Y,y

def loadBatch(batchname):
    d = unpickle(datasetFolder + batchname)
    X = d[b'data']
    # We have values between 0-255 we want
    # between 0 and 1
    X = X * (1/255)
    Y = np.zeros([X.shape[0], K])
    y = d[b'labels']
    # one-hot encoding
    for i in range(X.shape[0]):
        Y[i][y[i]] = 1
    return X,Y,y

#From where we got the image files
def unpickle(f):
    with open(f, 'rb') as fo:
        d = pickle.load(fo, encoding='bytes')
    return d

#function to check computed gradient vs analytic
def diffGradToComputed(gradW, gradB, compGradW, compGradB, X):
    eps = 0.0001

    print("for b vector")
    print("analytic:")
    print(gradB)
    print("computed:")
    print(compGradB)
    biggestB = 0
    totiB = 0
    for i in range(K):
        gagn = np.absolute(gradB[i,0] - compGradB[i,0])
        gapgn = np.absolute(gradB[i,0]) + np.absolute(compGradB[i,0])
        if gapgn < eps:
            div = eps
        else:
            div = gapgn
        res = gagn/div
        totiB += res
        if res > biggestB:
            biggestB = res
        print(res)
    totiB /= K
    print("for w matrix")
    biggestW = 0
    totiW = 0 
    for i in range(gradW.shape[0]):
        for j in range(gradW.shape[1]):
            gagn = np.absolute(gradW[i,j] - compGradW[i,j])
            gapgn = np.absolute(gradW[i,j]) + np.absolute(compGradW[i,j])
            if gapgn < eps:
                div = eps
            else:
                div = gapgn
            res = gagn/div
            totiW += res
            if res > biggestW:
                biggestW = res
            # print(res)
    totiW /= (K * X.shape[0])

    print("Biggest b diff was: %f and average %f" % (biggestB, totiB))
    print("Biggest w diff was: %f and average %f" % (biggestW, totiW))

#Numerical gradient, from the ComputeGradsNumSlow.m
def numericalGradientSlow(X, y, W, b, lamb, h):
    batchSize = X.shape[0]
    dimensions = X.shape[1]
    dldw = np.zeros(W.shape)
    dldb = np.zeros(b.shape)
    for i in range(K):
        b_try = copy.copy(b)
        b_try[i] = b_try[i] - h
        c1 = computeCost(X, y, W, b_try, lamb)
        b_try = copy.copy(b)
        b_try[i] = b_try[i] + h
        c2 = computeCost(X, y, W, b_try, lamb)
        dldb[i] = (c2-c1) / (2*h)

    for i,j in it.product(range(K), range(dimensions)):
        W_try = copy.copy(W)
        W_try[i][j] = W_try[i][j] - h
        cw1 = computeCost(X,y,W_try,b,lamb)

        W_try = copy.copy(W)
        W_try[i][j] = W_try[i][j] + h
        cw2 = computeCost(X,y,W_try,b,lamb)
        dldw[i][j] = (cw2-cw1) / (2*h)

    return(dldw, dldb)

'''
#code for checking the difference
(compGradW, compGradB) = numericalGradientSlow(X[:,:no], Y[:,:no],W,b,lamb,0.1)
#check the difference
diffGradToComputed(gradW, gradB, compGradW, compGradB, K, X)
'''

if __name__ == "__main__":
    main()
