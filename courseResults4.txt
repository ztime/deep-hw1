Running 50 experiments a 10 epochs
Eta = [0.010000, 0.015000]
Lambda = [0.000001, 0.000200]
----------------------------
Results sorted by Acc:
----------------------------
rand-course3-test26-VL1K9KA5: Eta: 0.014454 Lamda: 0.000099 Acc: 0.410600
rand-course3-test36-WQ826I7Z: Eta: 0.013666 Lamda: 0.000158 Acc: 0.407400
rand-course3-test30-OHZ1K3JJ: Eta: 0.013046 Lamda: 0.000019 Acc: 0.407000
rand-course3-test46-MXGZ3WP6: Eta: 0.011882 Lamda: 0.000189 Acc: 0.406800
rand-course3-test3-SBZV3KN5: Eta: 0.014569 Lamda: 0.000013 Acc: 0.406400
rand-course3-test32-JDX0Z1LU: Eta: 0.014581 Lamda: 0.000171 Acc: 0.406200
rand-course3-test24-0JDZHXMT: Eta: 0.010836 Lamda: 0.000113 Acc: 0.406100
rand-course3-test9-ZGI7PH8K: Eta: 0.011596 Lamda: 0.000188 Acc: 0.405400
rand-course3-test43-53NRWJEH: Eta: 0.011905 Lamda: 0.000160 Acc: 0.404900
rand-course3-test12-XNZVHMS2: Eta: 0.011077 Lamda: 0.000181 Acc: 0.403900
rand-course3-test13-ZD4EB2MO: Eta: 0.010972 Lamda: 0.000139 Acc: 0.403300
rand-course3-test37-AUYNUUFW: Eta: 0.011700 Lamda: 0.000092 Acc: 0.403300
rand-course3-test20-WK3QZMT4: Eta: 0.012908 Lamda: 0.000046 Acc: 0.403200
rand-course3-test41-7WJNWSP1: Eta: 0.011484 Lamda: 0.000153 Acc: 0.402500
rand-course3-test17-73TQD1CV: Eta: 0.011715 Lamda: 0.000131 Acc: 0.402300
rand-course3-test22-FTZO3JU0: Eta: 0.010872 Lamda: 0.000036 Acc: 0.402300
rand-course3-test33-O2A7140A: Eta: 0.013646 Lamda: 0.000105 Acc: 0.402200
rand-course3-test35-O2KR03WD: Eta: 0.014050 Lamda: 0.000164 Acc: 0.401700
rand-course3-test6-YDIWA3AT: Eta: 0.011999 Lamda: 0.000014 Acc: 0.401600
rand-course3-test45-7RHDDM12: Eta: 0.014643 Lamda: 0.000129 Acc: 0.401600
rand-course3-test23-UVWCCECA: Eta: 0.012183 Lamda: 0.000126 Acc: 0.401300
rand-course3-test49-GKF3BERQ: Eta: 0.010818 Lamda: 0.000059 Acc: 0.401200
rand-course3-test31-3HFQPVQ6: Eta: 0.013295 Lamda: 0.000089 Acc: 0.400800
rand-course3-test48-DFZGPLIE: Eta: 0.010711 Lamda: 0.000195 Acc: 0.400800
rand-course3-test1-2LIKN0C1: Eta: 0.012225 Lamda: 0.000025 Acc: 0.400700
rand-course3-test8-ZWNW35K3: Eta: 0.013795 Lamda: 0.000173 Acc: 0.400700
rand-course3-test11-BTYAFKST: Eta: 0.014238 Lamda: 0.000063 Acc: 0.400300
rand-course3-test15-PZWGZXVW: Eta: 0.012079 Lamda: 0.000181 Acc: 0.400000
rand-course3-test40-QX1EXO8D: Eta: 0.011625 Lamda: 0.000036 Acc: 0.399300
rand-course3-test16-PVW4EI0J: Eta: 0.011392 Lamda: 0.000106 Acc: 0.399200
rand-course3-test34-BGO5NPU0: Eta: 0.014588 Lamda: 0.000088 Acc: 0.399100
rand-course3-test5-G25OHTSC: Eta: 0.012645 Lamda: 0.000023 Acc: 0.399000
rand-course3-test42-MN224BHU: Eta: 0.010938 Lamda: 0.000084 Acc: 0.398900
rand-course3-test14-V5Q6PO2V: Eta: 0.010614 Lamda: 0.000046 Acc: 0.398600
rand-course3-test21-EUIM91FR: Eta: 0.013004 Lamda: 0.000177 Acc: 0.398500
rand-course3-test19-3DY7P9RK: Eta: 0.012317 Lamda: 0.000177 Acc: 0.398200
rand-course3-test4-DIH3LNS0: Eta: 0.011216 Lamda: 0.000101 Acc: 0.397700
rand-course3-test47-4SHQQCFN: Eta: 0.012339 Lamda: 0.000109 Acc: 0.397200
rand-course3-test44-1ES3WWH5: Eta: 0.014859 Lamda: 0.000151 Acc: 0.396800
rand-course3-test25-PVMO6GAT: Eta: 0.014526 Lamda: 0.000185 Acc: 0.396700
rand-course3-test7-UO5QAL5S: Eta: 0.010639 Lamda: 0.000090 Acc: 0.396200
rand-course3-test10-99ZCVGWP: Eta: 0.010590 Lamda: 0.000158 Acc: 0.394600
rand-course3-test38-WY968PPN: Eta: 0.013251 Lamda: 0.000146 Acc: 0.394200
rand-course3-test0-D7IDQ1YX: Eta: 0.013466 Lamda: 0.000182 Acc: 0.391900
rand-course3-test39-LNP19V4Z: Eta: 0.010407 Lamda: 0.000191 Acc: 0.391300
rand-course3-test2-Z2AB2YMY: Eta: 0.010544 Lamda: 0.000040 Acc: 0.391100
rand-course3-test28-K1CUGLB6: Eta: 0.010359 Lamda: 0.000137 Acc: 0.390800
rand-course3-test29-Y4VKSAZ4: Eta: 0.013199 Lamda: 0.000019 Acc: 0.390600
rand-course3-test27-DSYFNZPI: Eta: 0.010054 Lamda: 0.000155 Acc: 0.390500
rand-course3-test18-WCIM3WN6: Eta: 0.010706 Lamda: 0.000005 Acc: 0.387800
