Running 50 experiments a 5 epochs
Eta = [0.010000, 0.020000]
Lambda = [0.000010, 0.010000]
----------------------------
Results sorted by Acc:
----------------------------
rand-course3-test48-Y5YZ7C3U: Eta: 0.016818 Lamda: 0.000804 Acc: 0.371500
rand-course3-test15-TL76DKKU: Eta: 0.017056 Lamda: 0.000862 Acc: 0.368700
rand-course3-test18-B9Y5NSA0: Eta: 0.019131 Lamda: 0.005197 Acc: 0.368000
rand-course3-test49-SY73HXRI: Eta: 0.019668 Lamda: 0.002608 Acc: 0.367000
rand-course3-test3-HRBIQGUR: Eta: 0.015470 Lamda: 0.001660 Acc: 0.366700
rand-course3-test13-IBFZN9XK: Eta: 0.013614 Lamda: 0.001803 Acc: 0.366700
rand-course3-test38-OGEPPBIV: Eta: 0.016338 Lamda: 0.002887 Acc: 0.365600
rand-course3-test41-7GFDGXQ1: Eta: 0.017203 Lamda: 0.005808 Acc: 0.365200
rand-course3-test23-DF62KSSZ: Eta: 0.017499 Lamda: 0.000164 Acc: 0.363400
rand-course3-test5-2ZCM6G0U: Eta: 0.017137 Lamda: 0.001619 Acc: 0.362800
rand-course3-test29-NU2G4EFA: Eta: 0.018572 Lamda: 0.007817 Acc: 0.362400
rand-course3-test31-OOXLR9FQ: Eta: 0.019512 Lamda: 0.003379 Acc: 0.359700
rand-course3-test0-RVPROXM1: Eta: 0.016225 Lamda: 0.003206 Acc: 0.359300
rand-course3-test37-5TX0OIK4: Eta: 0.017278 Lamda: 0.004877 Acc: 0.358500
rand-course3-test14-SHI99SYM: Eta: 0.019829 Lamda: 0.006955 Acc: 0.358400
rand-course3-test39-RXDIUATK: Eta: 0.016863 Lamda: 0.002729 Acc: 0.358400
rand-course3-test27-CPOP200N: Eta: 0.019139 Lamda: 0.005781 Acc: 0.358300
rand-course3-test42-QYUFOYDO: Eta: 0.015838 Lamda: 0.000919 Acc: 0.357400
rand-course3-test43-0ZPYOKJM: Eta: 0.019880 Lamda: 0.007622 Acc: 0.357400
rand-course3-test44-WM9AM2VF: Eta: 0.013798 Lamda: 0.001974 Acc: 0.356300
rand-course3-test45-NO38I8TZ: Eta: 0.016756 Lamda: 0.008332 Acc: 0.355300
rand-course3-test40-B4QGV2QG: Eta: 0.019651 Lamda: 0.009978 Acc: 0.355000
rand-course3-test28-NNEHF245: Eta: 0.012364 Lamda: 0.000575 Acc: 0.354100
rand-course3-test6-L9DAABU4: Eta: 0.016398 Lamda: 0.008022 Acc: 0.351700
rand-course3-test10-41RZ9OD5: Eta: 0.016242 Lamda: 0.000224 Acc: 0.351200
rand-course3-test36-UKSCGLDG: Eta: 0.013079 Lamda: 0.004319 Acc: 0.350600
rand-course3-test20-HB9D724K: Eta: 0.015683 Lamda: 0.009454 Acc: 0.350100
rand-course3-test25-XNB2SX34: Eta: 0.015886 Lamda: 0.006833 Acc: 0.349800
rand-course3-test2-XMCDB8AJ: Eta: 0.013365 Lamda: 0.008771 Acc: 0.349300
rand-course3-test16-YNN105O5: Eta: 0.011268 Lamda: 0.001350 Acc: 0.349000
rand-course3-test19-3UVGPEVG: Eta: 0.014149 Lamda: 0.004059 Acc: 0.349000
rand-course3-test21-L4TD8U9Z: Eta: 0.013055 Lamda: 0.003637 Acc: 0.348600
rand-course3-test47-F1HK8STA: Eta: 0.013920 Lamda: 0.005772 Acc: 0.347900
rand-course3-test30-1ZWP8ELM: Eta: 0.013134 Lamda: 0.000556 Acc: 0.346600
rand-course3-test7-BMC3Y0ND: Eta: 0.018862 Lamda: 0.001301 Acc: 0.345200
rand-course3-test32-A5QMVKUD: Eta: 0.013253 Lamda: 0.001232 Acc: 0.345000
rand-course3-test33-E7ZGBWGO: Eta: 0.014470 Lamda: 0.007602 Acc: 0.344200
rand-course3-test12-00ZIVVXH: Eta: 0.013974 Lamda: 0.005842 Acc: 0.344000
rand-course3-test34-XLZYLSEQ: Eta: 0.014037 Lamda: 0.005556 Acc: 0.341500
rand-course3-test46-WVV4EHYO: Eta: 0.013258 Lamda: 0.006310 Acc: 0.340300
rand-course3-test17-76NNBHSY: Eta: 0.010261 Lamda: 0.001866 Acc: 0.338100
rand-course3-test35-LGC12MFS: Eta: 0.011751 Lamda: 0.000842 Acc: 0.337000
rand-course3-test1-6XD0PRRH: Eta: 0.017125 Lamda: 0.009550 Acc: 0.335500
rand-course3-test8-BG6U1SIA: Eta: 0.012707 Lamda: 0.009446 Acc: 0.333200
rand-course3-test24-8Z0SF5GE: Eta: 0.019986 Lamda: 0.009190 Acc: 0.331200
rand-course3-test26-SO9KEOYC: Eta: 0.011149 Lamda: 0.005667 Acc: 0.329100
rand-course3-test11-VZRZEJ18: Eta: 0.010387 Lamda: 0.003366 Acc: 0.328400
rand-course3-test9-QY09HFIW: Eta: 0.011685 Lamda: 0.008445 Acc: 0.324100
rand-course3-test4-7N45BSYM: Eta: 0.012078 Lamda: 0.007896 Acc: 0.322800
rand-course3-test22-MZKYCKAV: Eta: 0.010881 Lamda: 0.006946 Acc: 0.311600
